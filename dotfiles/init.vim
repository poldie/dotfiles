" https://gitlab.com/poldie/dotfiles.git
call plug#begin()
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.6' }
Plug 'nvim-lua/plenary.nvim'
Plug 'BurntSushi/ripgrep'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" if errors with :h 
" then run following line (https://github.com/neovim/neovim/issues/26515)
":TSInstall vimdoc -
call plug#end()

set background=dark
colorscheme industry

" Line numbers (relative, but show line number, not zero, on current line).
set number relativenumber
" How much > indents by.
set shiftwidth=4
" Do NOT continue search when bottom of document has been reached.
set nowrapscan
" Override 'ignorecase' when pattern has upper case characters.
set smartcase
" Wrap long lines (turn nowrap off!)
set nowrap!
" Highlight (or not) the screen line of the cursor.
"set cursorline
" 0, 1 or 2; when to use a status line for the last window.
" (Always display status bar).
set laststatus=2
" Mouse in all modes.
set mouse=a
" Start a dialog when a command fails.
set cf
" Display the current mode in the status line.
set showmode
" Show (partial / incomplete) command keys in the status line.
set showcmd
" Show cursor position below each window.
set ruler
" When inserting a bracket, do NOT briefly jump to its match.
set nosm
" Specifies what <BS>, CTRL-W, etc. can do in Insert mode.
set backspace=indent,eol,start
" List of flags that tell how automatic formatting works.
set formatoptions=lqrn1
" Linebreak - break as-is, not at end of whole word.
set lbr!
" Option settings for diff mode.
set diffopt=vertical
" Time to wait after ESC (default causes an annoying delay).
set timeoutlen=250
" How many command lines are remembered.
set history=256
" Command-line completion shows a list of matches.
set wildmenu
" Specifies how command line completion works.
set wildmode=list:longest,full
" Allow moving after the end of the line in visual block mode.
set virtualedit+=block
" Open new vertical windows to the right (not left).
set splitright
" Open new horizontal windows below the current one (not above).
set splitbelow
" Autocomplete: select longest word, always show menu.
set completeopt=longest,menuone
" Paths searched during certain operations.
set path=,,
" Tags default behaviour (look up from folder of current file until you find a
" tags file).
set tags=./tags;
" Set updatetime to lower than the default of 4000 (so tagbar updates which
" function we're in on time).
set updatetime=750
" Indicator at column 80.
set fillchars=fold:\ 
" Set Leader to be space.
let mapleader=" "

syntax enable

" block cursor for normal mode; thin cursor for insert mode
if exists('$TMUX')
 	let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
  	let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
else
"	let &t_SI = "\e[6 q"
"	let &t_EI = "\e[2 q"
endif

hi User1 ctermfg=14 ctermbg=16

" Statusline
" ----------
" Filename (absolute path-use f for relative).
set statusline=%1*\ %F\ 
" File format.
set statusline+=[%{&ff}]
" File type.
set statusline+=%y
" Modified flag.
set statusline+=%m
" Read only flag.
set statusline+=%r
" Help file flag.
set statusline+=%h
" Preview window flag.
set statusline+=%w
" Col number,line number
set statusline+=\ (%v,%l/%L\  
" Percentage through file, number of lines
set statusline+=%p%%)


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Keyboard mappings
" -----------------

" Using Lua functions
nnoremap <leader>/ <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>// <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>

" Produce a line of = chars of the same length as the current line,
" immediately below it.
nnoremap <leader>= YpVr=

" Same as above only for - chars.
nnoremap <leader>- YpVr-

"Shift-cursor keys resize windows.
nnoremap <S-Up> <C-w>+
nnoremap <S-Down> <C-w>-
nnoremap <S-Left> <C-w><
nnoremap <S-Right> <C-w>>

" In visual mode and :s/ typed (quickly), add \%V to search term.
" This ensures columnular blocks are operated on properly.
" (The default behaviour is to operate on the whole lines touched by columnular 
" selection, which is never what you'd want).
vnoremap :s/ :s/\%V 

" clipboard handling between vim and tmux on linux
 " copy text using tmux clipboard on linux
 " (couldn't use shift-backspace as tmux seems to treat that as control-h and
 " jumps between panes)

 " Yank selection
 vnoremap <bs> :Tyank<CR>
 " yank line
 nnoremap <bs> :Tyank<CR>
 " paste 
 nnoremap <bs><bs> :Tput<CR>

" Autocomplete: enter selects word.
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Autocomplete: send down key so we can keep typing and it'll locate nearest 
" match.
inoremap <expr> <C-n> pumvisible() ? '<C-n>' : '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

" j/k should navigate through word completion window.
inoremap <expr> j pumvisible() ? "\<C-N>" : "j"
inoremap <expr> k pumvisible() ? "\<C-P>" : "k"

" j and k should navigate a single row, even if we're on a wrapped line.
nnoremap j gj
nnoremap k gk

" Toggle wrap.
nnoremap <leader>w :set wrap!<cr>

" Open vimrc.
nnoremap <leader>v :e $MYVIMRC<cr>

" Navigate between buffers.
nnoremap <leader>, :bp<cr>
nnoremap <leader>. :bn<cr>
nnoremap [b :bp<cr>
nnoremap ]b :bn<cr>

" Navigate between tabs.
nnoremap [t :tabp<CR>
nnoremap ]t :tabn<CR>

" Navigate between ctags.
nnoremap [c :tp<CR>
nnoremap ]c :tn<CR>

" When navigating, goto the current line (via the mark p).
" This forces the current line into the jumps list, so you can then 
" control-i/control-o to navigate them like visual studio etc.
nnoremap <c-d> <c-d>mp`p
nnoremap <c-u> <c-u>mp`p
nnoremap <c-b> <c-b>mp`p
nnoremap <c-f> <c-f>mp`p
nnoremap L Lmp`p
nnoremap M Mmp`p
nnoremap H Hmp`p

" F1 unhighlights searched-for help.
nnoremap <F1> :noh<cr>
inoremap <F1> <esc>:noh<cr>a

" Leader f opens the file under the cursor in a new window.
" do it this way so vim-fetch can jump to the line number
nnoremap <leader>f <c-w>vgFzz

" vim-tmux-navigator
" ------------------
"
" use control-hjkl to move between panes in vim and tmux
" additional config in .tmux.conf

"let g:tmux_navigator_no_mappings = 1
"nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
"nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
"nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
"nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
"nnoremap <silent> {Previous-Mapping} :TmuxNavigatePrevious<cr>

" highlight yanked text
augroup highlight_yank
  autocmd!
  au TextYankPost * silent! lua vim.highlight.on_yank{higroup='IncSearch', timeout=500}
augroup END



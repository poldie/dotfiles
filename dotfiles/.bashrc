if [ -z "$PS1" ]; then
	return
fi

export LS_COLORS="no=00;38;5;244:rs=0:di=00;38;5;33:ln=01;38;5;37:mh=00:pi=48;5;230;38;5;136;01:so=48;5;230;38;5;136;01:do=48;5;230;38;5;136;01:bd=48;5;230;38;5;244;01:cd=48;5;230;38;5;244;01:or=48;5;235;38;5;160:su=48;5;160;38;5;230:sg=48;5;136;38;5;230:ca=30;41:tw=48;5;64;38;5;230:ow=48;5;235;38;5;33:st=48;5;33;38;5;230:ex=01;38;5;64:"
PS1='\[\e[0;37m\]\w\$ \[\e[0m\]'

# shell uses vi editing
set -o vi

# ctrl-d 3 times to disconnect (ie ignore 2 of them)
set -o ignoreeof
IGNOREEOF=2

# set term so we can use colourful tmux, vim etc
#export TERM="xterm-256color"

# default editor (apps use this)
export EDITOR=vim
export VISUAL=vim

# timezone
export TZ='Europe/London'

# autocd: cd without typing it!
shopt -s autocd
# correct typos in folder names during tab completion
shopt -s dirspell
# correct typos in folder names in arguments to cd
shopt -s cdspell

# ignore case on tab
bind "set completion-ignore-case on"
# treat . and _ identically on tab
bind "set completion-map-case on"
# don't make me hit tab twice to get list of matches
bind "set show-all-if-ambiguous on"

# handy aliases for cd-ing around
# up a level
alias ..='cd ..'
# home
alias ~='cd ~'
# back to last location
alias -- -='cd -'
# some of the above echo cwd - don't bother
alias cd='cd >/dev/null'

alias vim='nvim'
alias androidstudio='~/dev/tools/android-studio/bin/studio.sh >>/tmp/androidstudio.log 2>>/tmp/androidstudio.err &'

alias fail='tail -f'
alias jobs='jobs -l'

# handy ls-related aliases
#alias ls='ls -lh --color=auto'

#improved versions of old favourites!
#install htop
alias top='htop'
#install bat
alias cat='bat'
alias less='bat'
#install lsd
#also install "hack nerd font (mono regular) from https://www.nerdfonts.com/font-downloads
alias ls='lsd -l'
alias lt='lsd -l --tree'


##### cdable_vars #####

# enable cdable_vars:
# cd into a variable if it's not a folder
# set variables like this (ie don't use ~):
# export synapp="$HOME/dev/syndicate/synapp"
# then:
# cd synapp
# or, if you want tab completion:
# cd $synapp
# or just
# $synapp
shopt -s cdable_vars

# append, not overwrite, history file, when closing a session
shopt -s histappend

# enter to execute commands from hist (sudo !!, !text etc)
shopt -s histverify

# max size of history file (~/.bash_history) in lines
HISTSIZE=1000000

# max size of history file (~/.bash_history) in bytes
HISTFILESIZE=1000000

# don't store duplicates in history file (doesn't remove existing duplicates)
export HISTCONTROL=erasedups:ignoredups

# history command is not formatted with date and time
HISTTIMEFORMAT=""

# append history
PROMPT_COMMAND="history -a"

export PATH=$PATH:~/mypath

export FZF_DEFAULT_COMMAND='fd --type f'
export FZF_DEFAULT_OPTS="--layout=reverse --inline-info"

# to install fzf (because fedora repo is old)
# git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
# ~/.fzf/install
[ -f ~/.fzf.bash ] && source ~/.fzf.bash


